from optparse import OptionParser
from Bio import SeqIO
import re

# genetic code table
table = 1

parser = OptionParser()
parser.add_option("-f", "--file", action="store", dest = "filename", help = "input filename")
(options, args) = parser.parse_args()

masses = {
	"A" : 71.0788,
	"R" : 156.1875,
	"N" : 114.1038,
	"D" : 115.0886,
	"C" : 103.1388,
	"E" : 129.1155,
	"Q" : 128.1307,
	"G" : 57.0519,
	"H" : 137.1411,
	"I" : 113.1594,
	"L" : 113.1594,
	"K" : 128.1741,
	"M" : 131.1926,
	"F" : 147.1766,
	"P" : 97.1167,
	"S" : 87.0782,
	"T" : 101.1051,
	"W" : 186.2132,
	"Y" : 163.1760,
	"V" : 99.1326,
	"*" : 0
}

def getAllSubstrings(input_string, min_length):
  length = len(input_string)
  return [input_string[i:j+1] for i in xrange(0, length - min_length + 1) for j in xrange(i+min_length-1,length)]

def calculatePeptideMass(input_sequence):
	n = 18.015
	for aa in input_sequence:
		n += masses[aa]
	return n


def sixFrame(sequence):
	"""Function translate sequence in 6 frames
	Args:
	  sequence : nucleotide sequence
	Return:
	  list of six frames [strand, frame, sequence]
	"""
	frames = []
	for strand, nuc in [(+1, sequence.seq), (-1, sequence.seq.reverse_complement())]:
		for frame in range(3):
			peptide = nuc[frame:].translate(table)
			frames.append([strand, frame + (-strand + 1)/2 * 3 + 1, str(peptide)])
	return frames

def findORF(sequence):
	"""Function finds between stop sequences and returns them
	Args:
		sequence : amino acid sequence
	Return:
		list of amino acid ORFs
	"""
	orfs = []
	for m in re.finditer("[A-Z]{5,}\*", sequence):
		orfs.append(sequence[m.start():m.end()])
	for m in re.finditer("\*[A-Z]{1,}$", sequence):
		orfs.append(sequence[m.start() + 1:])
	return orfs

def substringPeptide(sequence):
	"""Function substring peptides by regular expressions
	Args:
	  sequence : 
	Return:
	  dictionary of peptides
	"""
	output = {}

	p_end = re.match('.*E[A-Z]{0,3}R([A-Z]*\*)', str(sequence))
	if p_end:
		p_end_seq = p_end.group(1)
		output["C-terminus"] = p_end_seq
	p_xxxr = re.match('.*E[A-Z]{0,3}R(.*E[A-Z]{0,3}R).*', str(sequence))
	if p_xxxr:
		p_xxxr_seq = p_xxxr.group(1)
		output["between_XXXXR"] = p_xxxr_seq
	return output

def modifications(sequence):
	"""Function return dictionary with modified masses
	Args:
	  sequence : 
	Return:
	  dictionary with modified masses
	"""
	mods = {}
	end_cut = ""
	# End cut
	end_regexp = re.compile("(WRR|GRR|LGK|IGK|FGK|WGR|SGR)\*")
	end_match = end_regexp.search(sequence)
	if end_match:
		end_cut = sequence[end_match.start():]
		sequence = sequence[:end_match.start() + 1]
	mass = calculatePeptideMass(sequence)
	mods["None"] = mass
	# Folding"
	n_CC = sequence.count("C")
	f4 = re.match("[A-Z]{1,}C[A-Z]{1,}CC[A-Z]{1,}C[A-Z]{1,}", sequence)
	f6 = re.match("[A-Z]{1,}C[A-Z]{1,}C[A-Z]{1,}CC[A-Z]{1,}C[A-Z]{1,}C[A-Z]{1,}", sequence)
	if (n_CC == 6 and f6 is not None) or (n_CC == 4 and f4 is not None):
		fold = -n_CC * 1.008
		mods["Fold"] = mass + fold
	# Amid
	amid = -0.984
	specific_end = ""
	amid_end_regexp = re.compile("(QSA|YKK|VKV|CKK|CK|YGD|FCV|AKA|PEK|FSK|WKL|GWT)\*")
	amid_end_match = amid_end_regexp.search(sequence)
	if amid_end_match is None:
		mods["Amid"] = mass + amid
	else:
		specific_end = sequence[amid_end_match.start():amid_end_match.end()]
	# remove *
	if re.match(".*\*", sequence):
		sequence = sequence[:-1]
	return [sequence, end_cut, specific_end, mods]


if not options.filename:
	parser.print_help()
else:
	print "ID\tstrand\tframe\ttype_of_peptide\tseq\tmod_seq\tend_cut\tspecific_end\tmod\tcalc_mass"
	for seq_record in SeqIO.parse(options.filename, "fasta"):
		seq_name = seq_record.id
		frames_seq = sixFrame(seq_record)
		orfs = []
		for strand, frame, seq in frames_seq:
			orfs_l = findORF(str(seq))
			for o in orfs_l:
				orfs.append([strand, frame, o])
		peptides = []
		for strand, frame, seq in orfs:
			speps = substringPeptide(seq)
			for sp in speps:
				peptides.append([strand, frame, sp, speps[sp]])
		peps_with_mods = []
		for strand, frame, pep_type, seq in peptides:
			pep_mods_l = modifications(seq)
			seq_mod = pep_mods_l[0]
			end_cut = pep_mods_l[1]
			specific_end = pep_mods_l[2]
			pep_mods = pep_mods_l[3]
			for mod in pep_mods:
				peps_with_mods.append([strand, frame, pep_type, seq, seq_mod, \
					end_cut, specific_end, mod, pep_mods[mod]])
			for out in peps_with_mods:
				print seq_name + "\t" + "\t".join(map(str, out))